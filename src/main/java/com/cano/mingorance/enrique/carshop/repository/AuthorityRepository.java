package com.cano.mingorance.enrique.carshop.repository;

import com.cano.mingorance.enrique.carshop.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
