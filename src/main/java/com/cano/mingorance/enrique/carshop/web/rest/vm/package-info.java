/**
 * View Models used by Spring MVC REST controllers.
 */
package com.cano.mingorance.enrique.carshop.web.rest.vm;
