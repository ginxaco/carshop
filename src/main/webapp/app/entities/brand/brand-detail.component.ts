import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IBrand } from 'app/shared/model/brand.model';

@Component({
  selector: 'jhi-brand-detail',
  templateUrl: './brand-detail.component.html'
})
export class BrandDetailComponent implements OnInit {
  brand: IBrand;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ brand }) => {
      this.brand = brand;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
