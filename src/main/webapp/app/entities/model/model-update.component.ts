import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IModel, Model } from 'app/shared/model/model.model';
import { ModelService } from './model.service';
import { IBrand } from 'app/shared/model/brand.model';
import { BrandService } from 'app/entities/brand';

@Component({
  selector: 'jhi-model-update',
  templateUrl: './model-update.component.html'
})
export class ModelUpdateComponent implements OnInit {
  isSaving: boolean;

  brands: IBrand[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    brand: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected modelService: ModelService,
    protected brandService: BrandService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ model }) => {
      this.updateForm(model);
    });
    this.brandService
      .query({ filter: 'model-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IBrand[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBrand[]>) => response.body)
      )
      .subscribe(
        (res: IBrand[]) => {
          if (!this.editForm.get('brand').value || !this.editForm.get('brand').value.id) {
            this.brands = res;
          } else {
            this.brandService
              .find(this.editForm.get('brand').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IBrand>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IBrand>) => subResponse.body)
              )
              .subscribe(
                (subRes: IBrand) => (this.brands = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(model: IModel) {
    this.editForm.patchValue({
      id: model.id,
      name: model.name,
      brand: model.brand
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const model = this.createFromForm();
    if (model.id !== undefined) {
      this.subscribeToSaveResponse(this.modelService.update(model));
    } else {
      this.subscribeToSaveResponse(this.modelService.create(model));
    }
  }

  private createFromForm(): IModel {
    return {
      ...new Model(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      brand: this.editForm.get(['brand']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IModel>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackBrandById(index: number, item: IBrand) {
    return item.id;
  }
}
