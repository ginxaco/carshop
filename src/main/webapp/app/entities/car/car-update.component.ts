import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICar, Car } from 'app/shared/model/car.model';
import { CarService } from './car.service';
import { IClient } from 'app/shared/model/client.model';
import { ClientService } from 'app/entities/client';
import { IModel } from 'app/shared/model/model.model';
import { ModelService } from 'app/entities/model';

@Component({
  selector: 'jhi-car-update',
  templateUrl: './car-update.component.html'
})
export class CarUpdateComponent implements OnInit {
  isSaving: boolean;

  clients: IClient[];

  models: IModel[];

  editForm = this.fb.group({
    id: [],
    color: [],
    client: [],
    model: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected carService: CarService,
    protected clientService: ClientService,
    protected modelService: ModelService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ car }) => {
      this.updateForm(car);
    });
    this.clientService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IClient[]>) => mayBeOk.ok),
        map((response: HttpResponse<IClient[]>) => response.body)
      )
      .subscribe((res: IClient[]) => (this.clients = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.modelService
      .query({ filter: 'car-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IModel[]>) => mayBeOk.ok),
        map((response: HttpResponse<IModel[]>) => response.body)
      )
      .subscribe(
        (res: IModel[]) => {
          if (!this.editForm.get('model').value || !this.editForm.get('model').value.id) {
            this.models = res;
          } else {
            this.modelService
              .find(this.editForm.get('model').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IModel>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IModel>) => subResponse.body)
              )
              .subscribe(
                (subRes: IModel) => (this.models = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  updateForm(car: ICar) {
    this.editForm.patchValue({
      id: car.id,
      color: car.color,
      client: car.client,
      model: car.model
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const car = this.createFromForm();
    if (car.id !== undefined) {
      this.subscribeToSaveResponse(this.carService.update(car));
    } else {
      this.subscribeToSaveResponse(this.carService.create(car));
    }
  }

  private createFromForm(): ICar {
    return {
      ...new Car(),
      id: this.editForm.get(['id']).value,
      color: this.editForm.get(['color']).value,
      client: this.editForm.get(['client']).value,
      model: this.editForm.get(['model']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICar>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackClientById(index: number, item: IClient) {
    return item.id;
  }

  trackModelById(index: number, item: IModel) {
    return item.id;
  }
}
