import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'brand',
        loadChildren: () => import('./brand/brand.module').then(m => m.CarshopBrandModule)
      },
      {
        path: 'model',
        loadChildren: () => import('./model/model.module').then(m => m.CarshopModelModule)
      },
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.CarshopClientModule)
      },
      {
        path: 'car',
        loadChildren: () => import('./car/car.module').then(m => m.CarshopCarModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CarshopEntityModule {}
