import { ICar } from 'app/shared/model/car.model';

export interface IClient {
  id?: number;
  name?: string;
  surname?: string;
  dni?: string;
  cars?: ICar[];
}

export class Client implements IClient {
  constructor(public id?: number, public name?: string, public surname?: string, public dni?: string, public cars?: ICar[]) {}
}
