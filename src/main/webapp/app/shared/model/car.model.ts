import { IClient } from 'app/shared/model/client.model';
import { IModel } from 'app/shared/model/model.model';

export interface ICar {
  id?: number;
  color?: string;
  client?: IClient;
  model?: IModel;
}

export class Car implements ICar {
  constructor(public id?: number, public color?: string, public client?: IClient, public model?: IModel) {}
}
