export interface IBrand {
  id?: number;
  name?: string;
  logoContentType?: string;
  logo?: any;
}

export class Brand implements IBrand {
  constructor(public id?: number, public name?: string, public logoContentType?: string, public logo?: any) {}
}
