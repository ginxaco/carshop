import { IBrand } from 'app/shared/model/brand.model';

export interface IModel {
  id?: number;
  name?: string;
  brand?: IBrand;
}

export class Model implements IModel {
  constructor(public id?: number, public name?: string, public brand?: IBrand) {}
}
