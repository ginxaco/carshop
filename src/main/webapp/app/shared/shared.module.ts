import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CarshopSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [CarshopSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [CarshopSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CarshopSharedModule {
  static forRoot() {
    return {
      ngModule: CarshopSharedModule
    };
  }
}
