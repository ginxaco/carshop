import { NgModule } from '@angular/core';

import { CarshopSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [CarshopSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [CarshopSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class CarshopSharedCommonModule {}
